#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from collections import defaultdict
from .base import Scraper, ExceptionMixin, StandardFactory, GetReleaseResultMixin, GetListResultMixin
from .base import SearchScraper as SearchScraperBase
from ..result import ListResult, NotFoundResult
import musicbrainzngs
import secret


READABLE_NAME = 'MusicBrainz'
SCRAPER_URL = 'http://musicbrainz.org/'
NOTES = 'For searches through YADG the [advanced query syntax](https://musicbrainz.org/doc/Indexed_Search_Syntax) is used.'

RATE_LIMIT = '1.0/s'
CONCURRENCY = 2


# initialize musicbrainzngs
musicbrainzngs.set_useragent('YADG', '0.1', secret.MUSICBRAINZ_CONTACT_EMAIL)


class MusicbrainzUtilityMixin(object):

    def _get_artist_name(self, artist_credit):
        artist_name = None
        if 'name' in artist_credit:
            artist_name = artist_credit['name']
        elif 'artist' in artist_credit and 'name' in artist_credit['artist']:
            artist_name = artist_credit['artist']['name']
        return artist_name

    def _get_medium_format_string(self, medium_list):
        format_bins = defaultdict(int)
        for medium in medium_list:
            if 'format' in medium:
                format_bins[medium['format']] += 1
        items = format_bins.items()
        items.sort(key=lambda x: x[0])
        format_components = []
        for format, count in items:
            format_component = ''
            if count > 1:
                format_component = u'%d\xd7' % count
            format_component += format
            if format_component:
                format_components.append(format_component)
        format_string = ' + '.join(format_components)
        return format_string

    def _ResponseError_to_unicode(self, exception):
        return str(exception).decode('ascii', 'ignore')

    def handle_initial_data_exception(self, exception):
        if isinstance(exception, musicbrainzngs.ResponseError):
            # the musicbrainzngs.ResponseError only provides a __str__ method, but the exception message can contain
            # a non utf-8 encoded bytestring. This will crash saving the task result in the database because the
            # database backends expect any bytestrings to be utf-8 encoded.
            # To circumvent this problem we forcefully decode the exception message to unicode.
            unicode_exception = self._ResponseError_to_unicode(exception)
            if self.check_special_exception_condition(unicode_exception):
                self.result = self.instantiate_result(self.get_special_exception_result_class())
            else:
                self.raise_exception(u"request to server unsuccessful: %s" % unicode_exception)
        else:
            raise exception


class ReleaseScraper(MusicbrainzUtilityMixin, GetReleaseResultMixin, Scraper, ExceptionMixin):

    _base_url = 'https://musicbrainz.org/'
    string_regex = '^http(?:s)?://(?:www\.)?musicbrainz.org/release/([A-Za-z0-9\-]+)$'

    def __init__(self, id):
        super(ReleaseScraper, self).__init__()
        self.id = id
        self.main_release_artists = []

    def get_instance_info(self):
        return u'id="%s"' % self.id

    def get_url(self):
        return self._base_url + 'release/%s' % self.id

    def _get_artists(self, artist_credits, include_main_artists=True):
        artists = []
        is_feature = False
        for artist_credit in artist_credits:
            if isinstance(artist_credit, dict):
                track_artist_name = self._get_artist_name(artist_credit)
                if track_artist_name:
                    artist = self.result.create_artist()
                    if track_artist_name == 'Various Artists':
                        artist.set_various(True)
                    else:
                        artist.set_name(track_artist_name)
                    if is_feature:
                        artist.append_type(self.result.ArtistTypes.FEATURING)
                    else:
                        artist.append_type(self.result.ArtistTypes.MAIN)
                    if is_feature or include_main_artists:
                        artists.append(artist)
            elif 'feat.' in artist_credit:
                is_feature = True
        return artists

    def add_release_event(self):
        if 'release-event-list' in self.data:
            for release_event in self.data['release-event-list']:
                date = None
                country = None
                if 'date' in release_event:
                    date = release_event['date']
                if 'area' in release_event and 'name' in release_event['area']:
                    country = release_event['area']['name']
                release_event = self.result.create_release_event()
                if date:
                    release_event.set_date(date)
                if country:
                    release_event.set_country(country)
                if date or country:
                    self.result.append_release_event(release_event)

    def add_release_format(self):
        components = []
        if 'medium-list' in self.data:
            format_string = self._get_medium_format_string(self.data['medium-list'])
            if format_string:
                components.append(format_string)
        if 'release-group' in self.data and 'primary-type' in self.data['release-group']:
            release_group = self.data['release-group']
            type_components = []
            release_type = release_group['primary-type']
            if release_type != '(unknown)':
                type_components.append(release_type)
            if 'secondary-type-list' in release_group:
                type_components.extend(release_group['secondary-type-list'])
            type_string = ' + '.join(type_components)
            if type_string:
                components.append(type_string)
        format = ', '.join(components)
        if format:
            self.result.set_format(format)

    def add_label_ids(self):
        if 'label-info-list' in self.data:
            for label_info in self.data['label-info-list']:
                if 'label' in label_info and 'name' in label_info['label']:
                    label_name = label_info['label']['name']
                    label_id = self.result.create_label_id()
                    label_id.set_label(label_name)
                    if 'catalog-number' in label_info:
                        label_id.append_catalogue_nr(label_info['catalog-number'])
                    self.result.append_label_id(label_id)

    def add_release_title(self):
        if 'title' in self.data:
            self.result.set_title(self.data['title'])

    def add_release_artists(self):
        if 'artist-credit' in self.data:
            artists = self._get_artists(self.data['artist-credit'])
            for artist in artists:
                self.result.append_release_artist(artist)
                if self.result.ArtistTypes.MAIN in artist.get_types():
                    self.main_release_artists.append(artist)

    def get_disc_containers(self):
        if 'medium-list' in self.data:
            medium_list = self.data['medium-list']
            return dict(zip([i for i in range(1, len(medium_list)+1)], medium_list))
        return {}

    def get_disc_title(self, disc_container):
        if 'title' in disc_container and disc_container['title']:
            return disc_container['title']
        return None

    def get_track_containers(self, disc_container):
        if 'track-list' in disc_container and disc_container['track-list']:
            return disc_container['track-list']
        else:
            return []

    def get_track_number(self, track_container):
        if not 'number' in track_container or not track_container['number']:
            self.raise_exception(u'could not find track number')
        return track_container['number']

    def get_track_artists(self, track_container, include_main_artists):
        track_artists = []
        if 'artist-credit' in track_container and track_container['artist-credit']:
            track_artists = self._get_artists(track_container['artist-credit'], include_main_artists)
        artist_map = dict(map(lambda x: (x.get_name(), x), track_artists))
        # there might be remixers attached to the recording
        if 'recording' in track_container and 'artist-relation-list' in track_container['recording']:
            for artist_rel in track_container['recording']['artist-relation-list']:
                if artist_rel.get('type', None) == 'remixer' and 'artist' in artist_rel:
                    artist = artist_rel['artist']
                    if 'name' in artist and artist['name']:
                        artist_name = artist['name']
                        # check if we have already added an artist with this name
                        if not artist_name in artist_map:
                            # if not we add a new one
                            remixer = self.result.create_artist()
                            remixer.set_name(artist_name)
                            remixer.append_type(self.result.ArtistTypes.REMIXER)
                            track_artists.append(remixer)
                            artist_map[artist_name] = remixer
                        elif not self.result.ArtistTypes.REMIXER in artist_map[artist_name].get_types():
                            # otherwise add the remixer type to the existing artist (it it does not already have it)
                            artist_map[artist_name].append_type(self.result.ArtistTypes.REMIXER)
        return track_artists

    def get_track_title(self, track_container):
        if 'title' in track_container:
            return track_container['title']
        elif 'recording' in track_container and 'title' in track_container['recording']:
            return track_container['recording']['title']
        else:
            self.raise_exception(u'could not find track title')

    def get_track_length(self, track_container):
        if 'length' in track_container:
            return int(round(float(track_container['length']) / float(1000)))
        return None

    def add_discs(self):
        disc_containers = self.get_disc_containers()

        # check if the main release artists are equal to all main track artists; we use this information later to skip
        # adding main track artists if each track has main artists that are exactly equal to the main release artists
        track_artists_equal_release_artists = True
        get_names = lambda x: x.get_name()
        main_release_artists_names = map(get_names, self.main_release_artists)
        for disc_nr in disc_containers:
            for track_container in self.get_track_containers(disc_containers[disc_nr]):
                track_artists = self.get_track_artists(track_container, True)
                main_track_artists = filter(lambda x: self.result.ArtistTypes.MAIN in x.get_types(), track_artists)
                main_track_artists_names = map(get_names, main_track_artists)
                track_artists_equal_release_artists &= main_release_artists_names == main_track_artists_names
                if not track_artists_equal_release_artists:
                    break
            if not track_artists_equal_release_artists:
                break

        for disc_nr in disc_containers:
            disc = self.result.create_disc()
            disc.set_number(disc_nr)

            disc_title = self.get_disc_title(disc_containers[disc_nr])
            if disc_title:
                disc.set_title(disc_title)

            for track_container in self.get_track_containers(disc_containers[disc_nr]):
                track = disc.create_track()
                track_number = self.get_track_number(track_container)
                if track_number:
                    track.set_number(track_number)
                track_title = self.get_track_title(track_container)
                if track_title:
                    track.set_title(track_title)
                track_length = self.get_track_length(track_container)
                if track_length:
                    track.set_length(track_length)
                track_artists = self.get_track_artists(track_container, not track_artists_equal_release_artists)
                for track_artist in track_artists:
                    track.append_artist(track_artist)

                disc.append_track(track)
            self.result.append_disc(disc)
            
    def get_initial_data(self):
        return musicbrainzngs.get_release_by_id(self.id, ["artists", "labels", "recordings", "release-groups", "media", "artist-credits", "artist-rels", "recording-level-rels"])['release']

    def check_special_exception_condition(self, exception):
        # MusicBrainz throws a 400 BAD REQUEST if the ID is malformed
        return 'HTTP Error 404' in exception or 'HTTP Error 400' in exception

    def get_special_exception_result_class(self):
        # so make sure we return a NotFoundResult in that case
        return NotFoundResult


class MusicbrainzSearchMixin(object):

    _base_url = 'https://musicbrainz.org/'

    def _get_artist_string(self, data):
        artist_string = ''
        if 'artist-credit-phrase' in data:
            artist_string = data['artist-credit-phrase']
        elif 'artist-credit' in data:
            components = []
            for artist_credit in data['artist-credit']:
                if isinstance(artist_credit, dict):
                    artist_name = self._get_artist_name(artist_credit)
                    if artist_name:
                        components.append(artist_name)
                else:
                    components.append(artist_credit)
            artist_string = ''.join(components)
        return artist_string

    def get_release_name(self, release_container):
        artist_string = self._get_artist_string(self._get_artist_string_data(release_container))
        title = release_container.get('title', None)
        if title is None:
            self.raise_exception(u'could not determine release title')
        name_components = []
        if artist_string:
            name_components.append(artist_string)
        if title:
            name_components.append(title)
        return u' \u2013 '.join(name_components)

    def get_release_url(self, release_container):
        release_id = release_container.get('id', None)
        if release_id:
            release_url = self._base_url + 'release/%s' % release_id
        else:
            release_url = None
        return release_url

    def get_release_info(self, release_container):
        components = []
        if 'medium-list' in release_container:
            format_string = self._get_medium_format_string(release_container['medium-list'])
            if format_string:
                components.append(format_string)
            tracknumber_components = []
            for medium in release_container['medium-list']:
                if 'track-count' in medium and medium['track-count']:
                    tracknumber_components.append(str(medium['track-count']))
            tracknumber_string = ' + '.join(tracknumber_components)
            if tracknumber_string:
                components.append('Tracks: ' + tracknumber_string)
        for key in ['date', 'country', 'barcode']:
            if key in release_container and release_container[key]:
                components.append(key[:1].upper() + key[1:] + ': ' + release_container[key])
        if 'label-info-list' in release_container and len(release_container['label-info-list']) > 0:
            label_info = release_container['label-info-list'][0]
            if 'label' in label_info and 'name' in label_info['label'] and label_info['label']['name']:
                components.append('Label: ' + label_info['label']['name'])
            if 'catalog-number' in label_info and label_info['catalog-number']:
                components.append('Cat#: ' + label_info['catalog-number'])
        info = u' | '.join(components)
        if info:
            return info
        return None


class ReleaseGroupScraper(MusicbrainzSearchMixin, MusicbrainzUtilityMixin, GetListResultMixin, Scraper, ExceptionMixin):

    string_regex = '^http(?:s)?://(?:www\.)?musicbrainz.org/release-group/([A-Za-z0-9\-]+)$'

    def __init__(self, id):
        super(ReleaseGroupScraper, self).__init__()
        self.id = id
        self.artist_string = None

    def get_instance_info(self):
        return u'id="%s"' % self.id

    def _get_artist_string(self, data):
        if self.artist_string is None:
            self.artist_string = super(ReleaseGroupScraper, self)._get_artist_string(data)
        return self.artist_string

    def _get_artist_string_data(self, release_container):
        return self.data

    def get_release_containers(self):
        if 'release-list' in self.data:
            return self.data['release-list']
        return []

    def get_initial_data(self):
        return musicbrainzngs.get_release_group_by_id(self.id, ["artists", "releases", "media"])['release-group']

    def check_special_exception_condition(self, exception):
        # MusicBrainz throws a 400 BAD REQUEST if the ID is malformed
        return 'HTTP Error 404' in exception or 'HTTP Error 400' in exception

    def get_special_exception_result_class(self):
        # so make sure we return an empty ListResult in that case
        return ListResult


class SearchScraper(MusicbrainzSearchMixin, MusicbrainzUtilityMixin, GetListResultMixin, SearchScraperBase, ExceptionMixin):

    def get_release_containers(self):
        if 'release-list' in self.data:
            return self.data['release-list']
        return []

    def _get_artist_string_data(self, release_container):
        return release_container

    def get_initial_data(self):
        return musicbrainzngs.search_releases(self.search_term, 25)

    def check_special_exception_condition(self, exception):
        # MusicBrainz throws a 400 BAD REQUEST if it cannot parse the query
        return 'HTTP Error 400' in exception

    def get_special_exception_result_class(self):
        # so make sure we return an empty ListResult in that case
        return ListResult


class ScraperFactory(StandardFactory):

    scraper_classes = [ReleaseScraper, ReleaseGroupScraper]
    search_scraper = SearchScraper

    global_rate_limit = RATE_LIMIT
    global_concurrency = CONCURRENCY