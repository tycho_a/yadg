#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import lxml
from .base import Scraper, GetReleaseResultMixin, GetListResultMixin, ExceptionMixin, RequestMixin, UtilityMixin, StandardFactory
from .base import SearchScraper as SearchScraperBase

READABLE_NAME = 'Beatport Pro'
SCRAPER_URL = 'https://pro.beatport.com/'


class ReleaseScraper(GetReleaseResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):
    string_regex = '^http(?:s)?://(?:(?:www|pro|classic)\.)?beatport\.com/release/(.*?)/(\d+)$'

    def __init__(self, id, release_name=''):
        super(ReleaseScraper, self).__init__()
        self.id = id
        self.release_name = release_name
        self._main_track_artists = None

    def get_instance_info(self):
        return u'id=%d release_name="%s"' % (self.id, self.release_name)

    @staticmethod
    def _get_args_from_match(match):
        if not match.group(1):
            return int(match.group(2)),
        return int(match.group(2)), match.group(1)

    def get_url(self):
        return 'https://pro.beatport.com/release/%s/%d' % (self.release_name, self.id)

    def get_headers(self):
        headers = super(ReleaseScraper, self).get_headers()
        headers['Accept-Language'] = 'en,en-US'
        return headers

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def _get_all_track_artists(self):
        if self._main_track_artists is None:
            self._main_track_artists = []
            main_track_artists = self.data.cssselect('ul.bucket-items li.track p.buk-track-artists a[data-artist]')
            main_track_artists = map(lambda x: self.remove_whitespace(x.text_content()), main_track_artists)
            # we are doing this less efficiently here than using sets to preserve the order of the track artists
            for main_track_artist in main_track_artists:
                if main_track_artist not in self._main_track_artists:
                    self._main_track_artists.append(main_track_artist)
        return self._main_track_artists

    def _get_release_chart_value_element(self, category):
        candidates = self.data.xpath("//li[@class='interior-release-chart-content-item']/span[@class='category' and text()='%s']/following-sibling::span" % category)
        if len(candidates) > 1:
            self.raise_exception(u'found more than one value element in release chart for category "%s"' % category)
        elif len(candidates) == 1:
            return candidates[0]
        return None

    def add_release_event(self):
        release_date_element = self._get_release_chart_value_element('Release Date')
        if release_date_element is not None:
            release_date = self.remove_whitespace(release_date_element.text_content())
            if release_date:
                release_event = self.result.create_release_event()
                release_event.set_date(release_date)
                self.result.append_release_event(release_event)

    def add_label_ids(self):
        labels_element = self._get_release_chart_value_element('Label')
        if labels_element is not None:
            catalog_element = self._get_release_chart_value_element('Catalog')
            if catalog_element is not None:
                # todo: investigate whether Beatport has releases with multiple catalogue numbers
                catalog_nr = self.remove_whitespace(catalog_element.text_content())
            else:
                catalog_nr = None
            label_as = labels_element.cssselect('a[data-label]')
            for label_a in label_as:
                label_name = self.remove_whitespace(label_a.text_content())
                if label_name:
                    label_id = self.result.create_label_id()
                    label_id.set_label(label_name)
                    if catalog_nr:
                        label_id.append_catalogue_nr(catalog_nr)
                    self.result.append_label_id(label_id)

    def add_release_title(self):
        title_h1 = self.data.cssselect('div.interior-release-chart-content > h1')
        if len(title_h1) != 1:
            self.raise_exception(u'could not find title element')
        title_h1 = title_h1[0]
        title = self.remove_whitespace(title_h1.text_content())
        if title:
            self.result.set_title(title)

    def add_release_artists(self):
        artists = self._get_all_track_artists()
        # we assume that it is a Various Artists release if the number of 'Artists' (not 'Remixers') is greater 3
        if len(artists) > 3:
            artist = self.result.create_artist()
            artist.set_various(True)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)
        else:
            for artist_name in artists:
                artist = self.result.create_artist()
                artist.set_name(artist_name)
                artist.append_type(self.result.ArtistTypes.MAIN)
                self.result.append_release_artist(artist)

    def add_genres(self):
        genre_elements = self.data.cssselect('ul.bucket-items li.track p.buk-track-genre a[data-genre]')
        genre_names = map(lambda x: self.remove_whitespace(x.text_content()), genre_elements)
        genre_names = set(genre_names)
        for genre in genre_names:
            self.result.append_genre(genre)

    def _create_artist(self, name):
        artist = self.result.create_artist()
        if name == 'Various Artists':
            artist.set_various(True)
        else:
            artist.set_name(name)
        return artist

    def get_track_artists(self, track_container):
        track_container = track_container[1]
        main_artist_as = track_container.cssselect('p.buk-track-artists a[data-artist]')
        if main_artist_as:
            get_artist_names = lambda x: self.remove_whitespace(x.text_content())
            additional_artist_as = track_container.cssselect('p.buk-track-remixers a[data-artist]')
            track_main_artists_names = map(get_artist_names, main_artist_as)
            track_additional_artists_names = map(get_artist_names, additional_artist_as)
            track_main_artists_names_set = set(track_main_artists_names)
            track_additional_artists_names_set = set(track_additional_artists_names)
            track_artists_intersection = track_main_artists_names_set.intersection(track_additional_artists_names_set)
            track_main_artists = map(self._create_artist, track_main_artists_names)
            track_additional_artists = map(self._create_artist, filter(lambda x: x not in track_artists_intersection, track_additional_artists_names))
            for track_main_artist in track_main_artists:
                track_main_artist.append_type(self.result.ArtistTypes.MAIN)
                if track_main_artist.get_name() in track_artists_intersection:
                    track_main_artist.append_type(self.result.ArtistTypes.REMIXER)
            for track_additional_artist in track_additional_artists:
                track_additional_artist.append_type(self.result.ArtistTypes.REMIXER)
            if track_main_artists == self.result.get_release_artists():
                track_artists = track_additional_artists
            else:
                track_artists = track_main_artists + track_additional_artists
            return track_artists
        return []

    def get_track_title(self, track_container):
        track_container = track_container[1]
        track_title_element = track_container.cssselect('p.buk-track-title a')
        if len(track_title_element) == 1:
            track_title_element = track_title_element[0]
            primary_title = track_title_element.cssselect('span.buk-track-primary-title')
            if len(primary_title) == 1:
                track_title = self.remove_whitespace(primary_title[0].text_content())
                remix_title = track_title_element.cssselect('span.buk-track-remixed')
                if len(remix_title) == 1:
                    remix_name = self.remove_whitespace(remix_title[0].text_content())
                else:
                    remix_name = None
                if track_title and remix_name and remix_name.lower() != 'original mix':
                    track_title += u' [' + remix_name + u']'
                if track_title:
                    return track_title
        return None

    def get_track_length(self, track_container):
        track_container = track_container[1]
        length_element = track_container.cssselect('p.buk-track-length')
        if len(length_element) == 1:
            track_duration = self.remove_whitespace(length_element[0].text_content())
            if track_duration:
                return self.seconds_from_string(track_duration)
        return None

    def get_track_number(self, track_container):
        return track_container[0]

    def get_disc_containers(self):
        disc_container = self.data.cssselect('div.tracks ul.bucket-items')
        if len(disc_container) != 1:
            self.raise_exception(u'could not get disc container')
        return {1: disc_container[0]}

    def get_track_containers(self, disc_container):
        track_containers = disc_container.cssselect('li.track')
        return zip(map(str, range(1, len(track_containers)+1)), track_containers)


class SearchScraper(GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):
    url = 'https://pro.beatport.com/search/releases'

    def get_params(self):
        return {'q': self.search_term}

    def get_url(self):
        return self.url

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def get_release_containers(self):
        return self.data.cssselect('div.releases ul.bucket-items li.release')[:25]

    def get_release_name(self, release_container):
        name_components = []
        release_artists_element = release_container.cssselect('p.release-artists')
        if len(release_artists_element) == 1:
            release_artists = self.remove_whitespace(release_artists_element[0].text_content())
            if release_artists:
                name_components.append(release_artists)
        release_title_element = release_container.cssselect('p.release-title')
        if len(release_title_element) == 1:
            release_title = self.remove_whitespace(release_title_element[0].text_content())
            if release_title:
                name_components.append(release_title)
        name = u' \u2013 '.join(name_components)
        if name:
            return name
        return None

    def get_release_info(self, release_container):
        release_label_element = release_container.cssselect('p.release-label')
        if len(release_label_element) == 1:
            label = self.remove_whitespace(release_label_element[0].text_content())
            if label:
                return u'Label: %s' % label
        return None

    def get_release_url(self, release_container):
        release_link = release_container.cssselect('div.release-meta > a')
        if len(release_link) == 1:
            url = self.remove_whitespace(release_link[0].attrib['href'])
            if url.startswith('/'):
                url = 'https://pro.beatport.com' + url
            if url:
                return url
        return None


class ScraperFactory(StandardFactory):
    scraper_classes = [ReleaseScraper]
    search_scraper = SearchScraper