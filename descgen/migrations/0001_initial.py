#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DependencyClosure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('path_length', models.IntegerField()),
                ('count', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('default_scraper', models.CharField(blank=True, max_length=40, null=True, help_text=b'This scraper will be selected by default in the scraper dropdown menu.', choices=[(b'bandcamp', b'Bandcamp'), (b'discogs', b'Discogs'), (b'itunes', b'iTunes Store'), (b'junodownload', b'Junodownload'), (b'metalarchives', b'Metal-Archives'), (b'musicbrainz', b'MusicBrainz'), (b'musiksammler', b'Musik-Sammler')])),
            ],
            options={
                'verbose_name_plural': 'settings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subscriber', models.ForeignKey(related_name='subscribed_to', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(related_name='subscriber_set', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Enter the name of the template here.', max_length=40)),
                ('template', models.TextField(help_text=b'Enter the template code here.', blank=True, validators=[django.core.validators.MaxLengthValidator(8192)])),
                ('is_public', models.BooleanField(default=False, help_text=b'Make this template public. Public templates can be used by all users that are subscribed to you.')),
                ('is_default', models.BooleanField(default=False, help_text=b'Make this template a default. Default templates can be used by all registered users and users that are not logged in.')),
                ('is_utility', models.BooleanField(default=False, help_text=b"Mark this template as a utility. Utility templates are only used as the basis for other templates and won't appear in the list of available templates for rendering a release directly.")),
                ('dependencies', models.ManyToManyField(help_text=b'Choose which templates this template depends on. Chosen templates can be included or extended in your template code.', related_name='depending_set', to='descgen.Template', blank=True)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='template',
            unique_together=set([('owner', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('user', 'subscriber')]),
        ),
        migrations.AddField(
            model_name='settings',
            name='default_template',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='descgen.Template', help_text=b'This template will be the default template when viewing a result.', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='settings',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, help_text=b'The user these settings belong to. If left blank these settings will apply to every unauthenticated user'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dependencyclosure',
            name='ancestor',
            field=models.ForeignKey(related_name='closure_ancestor', to='descgen.Template'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dependencyclosure',
            name='descendant',
            field=models.ForeignKey(related_name='closure_descendant', to='descgen.Template'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='dependencyclosure',
            unique_together=set([('descendant', 'ancestor', 'path_length')]),
        ),
    ]
